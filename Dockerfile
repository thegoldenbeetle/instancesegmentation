FROM nvidia/cuda:11.4.1-devel-ubuntu20.04

ENV DUSER seg
ENV DPASSWORD seg

RUN apt-get update -y \
    && apt-get install -y wget \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -ms /bin/bash ${DUSER} \ 
    && echo "${DUSER}:${DPASSWORD}" | chpasswd
WORKDIR /app
RUN chown ${DUSER}:${DUSER} /app

USER ${DUSER}

# Copy package
COPY --chown=${DUSER}:${DUSER} . /app

# Install Miniconda
ENV CONDA_PATH /home/${DUSER}/miniconda3
ENV PATH $CONDA_PATH/bin:$PATH
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && bash Miniconda3-latest-Linux-x86_64.sh -fbp $CONDA_PATH \
    && rm Miniconda3-latest-Linux-x86_64.sh

# Create and activate conda environment
RUN conda init bash \
    && $CONDA_PATH/bin/conda env create --file=seg_conda_env.yaml \
    && echo "conda activate seg" >> ~/.bashrc \
    && conda clean -a

# Forward jupyter
RUN conda run -n seg jupyter notebook --generate-config \
    && echo "c.NotebookApp.ip = '0.0.0.0'" >> /home/${DUSER}/.jupyter/jupyter_notebook_config.py



