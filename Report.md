## Instance сегментация людей
### 1. Предобученный Mask R-CNN дообучить на части COCO

**Обучающий датасет**: COCO train, часть только с людьми ( 64115 / 117266 ).

**Датасет для тестирования**: COCO val, часть только с людьми ( 2693 / 4952 ).

Обучающий датасет был разбит на train и validation (2000). Для обучения каждую эпоху выбиралось 3000 изображений из train случайным образом. 

**Аугментации**: изображения пропорционально уменьшались до 256*256, затем применялся random horizontal flip.

Эксперименты:

1. Дообучение только box_predictor и mask_predictor не дало результатов.
2. Дообучение box_predictor, mask_predictor и 2 последних размороженных слоя backbone дало практически такие же результаты, как и модель до обучения.
```python
# Параметры обучения
optimizer = torch.optim.SGD(params, lr=0.00005, momentum=0.9, weight_decay=0.0005)
batch_size = 1
```
| ![Alt text](./images/plot_ap_coco.png) | ![Alt text](./images/plot_ar_coco.png) |
| :---------------------------: | :----------------------------: |

**Среднее время инференса:** 322ms

Примеры работы:

| ![Alt text](./images/img.png) | ![Alt text](./images/img2.png) |
| :---------------------------: | :----------------------------: |
|             335ms             |             354ms              |

### 2. Предобученный Mask R-CNN дообучить на части Cityscapes

**Обучающий датасет**: Cityscapes train, часть только с person и rider, person заменен на rider ( 2476 / 2975 ).

**Датасет для тестирования**: Cityscapes val, часть часть только с person и rider, person заменен на rider ( 439 / 500 ).

Обучающий датасет был разбит на train и validation (476).  Обучение проводилось по батчам из 4ех изображений.

Осуществлялось дообучение box_predictor, mask_predictor и 3 последних размороженных слоя backbone.

**Аугментации**: random horizontal flip.

Эксперименты:

1. 
```python
# Параметры обучения
optimizer = torch.optim.SGD(params, lr=0.001, momentum=0.9, weight_decay=0.0005)
lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=2, gamma=0.1)
batch_size = 4
```
**Среднее время инференса:** 357ms

2. 
```python
# Параметры обучения
optimizer = torch.optim.Adam(params, lr=1e-4)
batch_size = 4
```
**Среднее время инференса:** 337ms

| ![Alt text](./images/plot_ap_3.png) | ![Alt text](./images/plot_ar_3.png) |
| :---------------------------: | :----------------------------: |

Примеры работы:

| mask_rcnn_cityscapes_training_2_4 | mask_rcnn_cityscapes_training_adam_7 |
| :---------------------------: | :----------------------------: |
| ![Alt text](./images/img_.png)| ![Alt text](./images/img3.png) |
|             466ms             |             414ms              |
| ![Alt text](./images/img2_.png)| ![Alt text](./images/img4.png) |
|             327ms             |             324ms              |

