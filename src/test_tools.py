import sys
import torch
import time

sys.path.insert(0, './../')
from references_detection import utils
from references_detection.engine import evaluate
from src.model import get_model_instance_segmentation


@torch.no_grad()
def get_inference_time(img, model, device):
    '''
    Измерение времени инференса для прохода по одному изображению.
    '''
    img = img.to(device)
    model = model.to(device)
    model = model.eval()
    
    start = time.time()
    pred = model([img])
    if torch.cuda.is_available() and device == torch.device('cuda'):
        torch.cuda.synchronize()
    end = time.time()
    
    return end - start


@torch.no_grad()
def get_model_prediction_one_image(img, model, device):
    '''
    Предсказание модели для одного входного изображения.

        Параметры:
            img: изображение должно быть тензором [C, H, W] со значениями в интервале [0..1]
            model: модель, осуществляющая предсказание. Предсказание должно
                   быть словарем с ключами scores, masks, labels, boxes
            device: устройство для вычислений, torch.device

        Возвращаемое значение:
            словарь, содержащий scores, masks, labels, boxes
    '''
    img = img.to(device)
    model = model.to(device)

    model = model.eval()

    pred = model([img])
    
    return {
        k: v.detach().cpu().numpy()
        for k, v in pred[0].items()
        if k in ['scores', 'masks', 'labels', 'boxes']
    }


def test_model_on_dataset(model, dataset, device, batch_size=1, cat_ids=None, print_freq=100):
    '''
    Тестирование модели на переданном датасете.
    '''
    model = model.to(device)
    data_loader = torch.utils.data.DataLoader(dataset, batch_size, shuffle=False, \
                                              collate_fn=utils.collate_fn)

    return evaluate(model, data_loader, device=device, cat_ids=cat_ids, print_freq=print_freq)


def test_models(models, datasets, device):
    '''
    Тестирование нескольких моделей на нескольких датасетах с сохранением результатов в словарь.
    
        Параметры:
            models: список с моделями
            datasets: словарь, ключ - тег датасета, значение- датасет
    '''
    result_dict = {}
    for model_tag, model in models:
        result_dict[model_tag] = dict()
        for dataset_tag, dataset in datasets.items():
            results, time = test_model_on_dataset(model, dataset, device, print_freq=500)
            result_dict[model_tag][dataset_tag] = dict()
            result_dict[model_tag][dataset_tag]['metrics'] = results.stats_dict()
            result_dict[model_tag][dataset_tag]['time'] = time
        if device == torch.device('cuda') and torch.cuda.is_available():
            model = model.to(torch.device('cpu'))
            torch.cuda.synchronize()
    return result_dict