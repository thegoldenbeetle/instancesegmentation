import torch

from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.mask_rcnn import MaskRCNNPredictor
from torchvision.models.detection import maskrcnn_resnet50_fpn


def get_model_instance_segmentation(num_classes, hidden_layer=256, trainable_backbone_layers=0, pretrained=True):
    '''
    Возвращает предобученную модель Mask R-CNN с измененными box и mask predictors
    в соответствии с заданным в параметре num_classes числом классов
    '''
    
    model = maskrcnn_resnet50_fpn(pretrained=pretrained, trainable_backbone_layers=trainable_backbone_layers)

    in_features = model.roi_heads.box_predictor.cls_score.in_features
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)

    in_features_mask = model.roi_heads.mask_predictor.conv5_mask.in_channels
    model.roi_heads.mask_predictor = MaskRCNNPredictor(in_features_mask,
                                                       hidden_layer,
                                                       num_classes)

    return model


def load_model(num_classes, model_path):
    model = get_model_instance_segmentation(num_classes)
    model.load_state_dict(torch.load(model_path))
    return model
