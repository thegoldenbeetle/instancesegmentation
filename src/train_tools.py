import sys
import torch

from .visualization_tools import show_instance_segmentation_by_model_predictions, get_float_image
from .test_tools import get_model_prediction_one_image

sys.path.insert(0, './../')
from references_detection.engine import evaluate
from references_detection.engine import train_one_epoch


def train_cycle(model, num_epochs, optimizer, device, data_loader_train, data_loader_val, writer, tensorboard_tag,\
                model_save_path, lr_scheduler=None, eval_images=None, print_freq=100, save_model_freq=1):
    '''
    Обучающий цикл. Сохраняет результаты обучения в tensorboard по переданному
    writer. Результаты могут содержать предсказания для файлов с изображениями 
    из eval_images. Печатает логи со значениями loss функций с частотой print_freq для каждой эпохи,
    сохраняет модель в папку model_save_path с именем tensorboard_tag_epoch_num.pth каждую
    save_model_freq эпоху.
    '''
    for epoch in range(num_epochs):
        
        train_res = train_one_epoch(model, optimizer, data_loader_train, device, epoch, print_freq=print_freq)
        for k, v in train_res.meters.items():
            writer.add_scalar('train: /' + k, v.global_avg, global_step=epoch)
        
        if lr_scheduler is not None:
            lr_scheduler.step()
        
        if epoch % save_model_freq == 0:
            torch.save(model.state_dict(), model_save_path / (tensorboard_tag + "_" + str(epoch) + ".pth"))
        
        eval_res, _ = evaluate(model, data_loader_val, device=device, print_freq=print_freq)
        for k, v in eval_res.stats_dict().items():
            writer.add_scalar('valid: /' + k, v, global_step=epoch)
        
        if eval_images is not None:
            for image_path in eval_images:
                img = get_float_image(image_path)
                pred = get_model_prediction_one_image(img, model, device)
                fig = show_instance_segmentation_by_model_predictions(\
                        img, pred, score_thres=0.5, mask_thres=0.5, classes=[1]\
                )
                writer.add_figure('test images: /' + image_path.name, fig, global_step=epoch)
