Предобученный mask rcnn обучен на части coco с людьми. 
Каждую эпоху берется Random sampler размера ```epoch_size``` из обучающей подвыборки.

Датасет:

```python
coco_dataset_path = "COCO/train2017"
coco_person_annotations_train = "COCO/annotations_trainval2017/annotations/person_annotations/train.json"
```
Параметры:
```python
num_epochs = 8
num_classes = 2
val_num = 2000
epoch_size = 3000 
batch_size = 1
optimizer = torch.optim.Adam(params, lr=1e-4)
train_transforms = [TL.Resize(size=(256, 256)), TL.RandomHorizontalFlip()]
model = get_model_instance_segmentation(num_classes, trainable_backbone_layers=2)
```

