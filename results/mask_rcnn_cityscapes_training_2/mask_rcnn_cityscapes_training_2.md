Предобученный mask rcnn обучен на части cityscapes с людьми. 
Обучался по батчам, в каждом по 4 изображения

Датасет:
```python
cityscapes_dataset_path = "cityscapes"
cityscapes_annotations_train = "cityscapes/annotations/person_annotations_train.json"
```
Параметры:

```python
num_epochs = 6
num_classes = 2
batch_size = 4
val_num = 476
optimizer = torch.optim.SGD(params, lr=0.001, momentum=0.9, weight_decay=0.0005)
lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=2, gamma=0.1)
train_transforms = [TL.RandomHorizontalFlip()]
model = get_model_instance_segmentation(num_classes, trainable_backbone_layers=3)
```

