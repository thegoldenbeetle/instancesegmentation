Предобученный mask rcnn обучен на части coco с людьми. 
Каждую эпоху берется Random sampler размера ```epoch_size``` из обучающей подвыборки.

Датасет:

```python
coco_dataset_path = "COCO/train2017"
coco_person_annotations_train = "COCO/annotations_trainval2017/annotations/person_annotations/train.json"
```
Параметры:
```python
num_epochs = 10
num_classes = 2
val_num = 2000
epoch_size = 3000 
batch_size = 1
optimizer = torch.optim.SGD(params, lr=0.005, momentum=0.9, weight_decay=0.0005)
lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=2, gamma=0.1)
train_transforms = [TL.Resize(size=(256, 256)), TL.RandomHorizontalFlip()]
model = get_model_instance_segmentation(num_classes, trainable_backbone_layers=0)
```

