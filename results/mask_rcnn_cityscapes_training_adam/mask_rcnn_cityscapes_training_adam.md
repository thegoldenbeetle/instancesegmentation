Предобученный mask rcnn обучен на части cityscapes с людьми. 
Обучался по батчам, в каждом по 4 изображения

Датасет:
```python
cityscapes_dataset_path = "cityscapes"
cityscapes_annotations_train = "cityscapes/annotations/person_annotations_train.json"
```
Параметры:

```python
num_epochs = 6
num_classes = 2
batch_size = 4
val_num = 476
optimizer = torch.optim.Adam(params, lr=1e-4)
train_transforms = [TL.RandomHorizontalFlip()]
model = get_model_instance_segmentation(num_classes, trainable_backbone_layers=3)
```

